from web.models import News
from web.models import NewsPluginModel
from web.models import DealsPluginModel
from web.models import TopicPluginModel
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _


class DealsPlugin(CMSPluginBase):
    model = DealsPluginModel
    name = _("Deals Plugin")
    render_template = "deals/plugin.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance.deals.deal_set.all()})
        return context


class TopicPlugin(CMSPluginBase):
    model = TopicPluginModel
    name = _("Topic Plugin")
    render_template = "topic/plugin.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance.topics.topic_set.all()})
        return context


class NewsPlugin(CMSPluginBase):
    model = NewsPluginModel
    name = _("News Plugin")
    render_template = "news/plugin.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': News.objects.filter(published=True)})
        return context


plugin_pool.register_plugin(NewsPlugin )
plugin_pool.register_plugin(DealsPlugin)
plugin_pool.register_plugin(TopicPlugin)
