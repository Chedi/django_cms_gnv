import haystack
from cms.models import monkeypatch_reverse

monkeypatch_reverse()
haystack.autodiscover()
