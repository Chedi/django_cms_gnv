from web import models
from django.shortcuts import redirect
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer, JSONPRenderer


def LandingPage(request):
    if 'HTTP_ACCEPT_LANGUAGE' in request.META:
        del request.META['HTTP_ACCEPT_LANGUAGE']
    return redirect('/fr/')


class WebServiceDetail(APIView):
    renderer_classes = (TemplateHTMLRenderer,)


    def get(self, request, *args, **kwargs):
        news  = models.News.objects.filter(published=True, sidebar=True)
        deals = models.Deal.objects.filter(published=True, sidebar=True)
        return Response({
            'news' : news,
            'deals': deals,
             }, template_name='sidebar.html')


class NewsletterCOS(APIView):
    render_classes = (JSONPRenderer,)

    def post(self, request):
        import httplib2
        import json
        import xmltodict

	cos_url = request.POST.get('cos_url')

        h = httplib2.Http(disable_ssl_certificate_validation=True)
        resp, content = h.request(cos_url, "GET")

        result = json.dumps(xmltodict.parse(content))
	return Response(result)
