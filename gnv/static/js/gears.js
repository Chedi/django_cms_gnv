$(document).ready(function(){
	$('.customSelect .opKeeper li label input').on(
		'click',
		function(e){
			e.preventDefault();
			e.stopImmediatePropagation();

			var input = $(this);
			var customSelect = input.parents('.customSelect');

			if(input.is(":checked")){
				customSelect.find('.display').html( input.val() );
			}
		}
	)
	$('.customSelect .trigger').on(
		"click",
		function(e){
			e.preventDefault();
			e.stopImmediatePropagation();

			var trigger = $(this);
			var customSelect = trigger.parents('.customSelect');

			if(customSelect.is('.open')){
				$('.customSelect').removeClass('open');
			}else{
				$('.customSelect').removeClass('open');
				customSelect.addClass('open');

				$('body').unbind('click').click(function(e){
					$('.customSelect').removeClass('open');
					$(this).unbind('click');
				});
			}
		}
	);
	
	$('#sideBar').load('http://' + window.location.hostname + '/sidebar/');

});